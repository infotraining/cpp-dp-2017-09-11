#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "factory.hpp"

using namespace std;

class Client
{
    ServiceCreator creator_;

public:
    Client(ServiceCreator creator) : creator_(creator)
    {
    }

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
    {
        unique_ptr<Service> service = creator_();

        string result = service->run();
        cout << "Client is using: " << result << endl;
    }
};

typedef std::map<std::string, ServiceCreator> Factory;

int main()
{
    Factory creators;
    creators.insert(make_pair("ServiceA", [] { return std::make_unique<ConcreteServiceA>();}));
    creators.insert(make_pair("ServiceB", &make_unique<ConcreteServiceB>));
    creators.insert(make_pair("ServiceC", &make_unique<ConcreteServiceC>));

    Client client(creators["ServiceC"]);
    client.use();

    vector<int> vec = { 1, 2, 3 };
    for(auto item : vec)
    {
        //...
    }

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        auto item = *it;
        //...
    }
}
