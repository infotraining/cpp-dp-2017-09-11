#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.hpp"

namespace Drawing
{
    class Circle : public ShapeBase
    {
        int radius_;
    public:
        constexpr static const char* id = "Circle";

        Circle(int x = 0, int y = 0, int radius = 0) : ShapeBase(x, y), radius_{radius}
        {}

        int radius() const
        {
            return radius_;
        }

        void set_radius(int r)
        {
            radius_ = r;
        }

        void draw() const override
        {
            std::cout << "Drawing a circle at " << coord() << " with r: " << radius() << std::endl;
        }
    };
}

#endif // CIRCLE_H
