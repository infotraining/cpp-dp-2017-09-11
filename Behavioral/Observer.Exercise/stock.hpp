#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <iostream>
#include <string>
#include <set>
#include <boost/signals2.hpp>

// Subject
class Stock
{
public:
    using PriceChangedSignal = boost::signals2::signal<void (std::string, double)>;
    using PriceChangedSlot = PriceChangedSignal::slot_type;
private:
    std::string symbol_;
    double price_;

    // signal
    PriceChangedSignal price_changed_;

public:
    Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
    {
    }

    std::string get_symbol() const
    {
        return symbol_;
    }

    double get_price() const
    {
        return price_;
    }

    // TODO: rejestracja obserwatora
    boost::signals2::connection connect(const PriceChangedSlot& slot )
    {
        return price_changed_.connect(slot);
    }

    void set_price(double price)
    {
        if (price_ != price)
        {
            price_ = price;
            price_changed_(symbol_, price);
        }
    }
};

class Investor
{
    std::string name_;

public:
    Investor(const std::string& name) : name_(name)
    {
    }

    void update_portfolio(std::string symbol, double price)
    {
        std::cout << name_ << " has been notified: " << symbol << " - " << price << "$" << std::endl;

        //...
    }
};

#endif /*STOCK_HPP_*/
