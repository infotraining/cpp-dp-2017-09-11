#include "shape_group_reader_writer.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered =
            SingletonShapeRWFactory::instance()
                .register_creator(make_type_index<ShapeGroup>(),
                                  [] { return make_unique<ShapeGroupReaderWriter>(
                                            SingletonShapeFactory::instance(),
                                            SingletonShapeRWFactory::instance()); });
}

void ShapeGroupReaderWriter::read(Shape& shp, istream& in)
{
    ShapeGroup& sg = static_cast<ShapeGroup&>(shp);

    int count;
    in >> count;

    for(int i = 0; i < count; ++i)
    {
        string type_id;
        in >> type_id;

        auto shp = shape_factory_.create(type_id);
        auto shp_rw = shape_rw_factory_.create(make_type_index(*shp));
        shp_rw->read(*shp, in);

        sg.add(move(shp));
    }
}

void ShapeGroupReaderWriter::write(const Shape& shp, ostream& out)
{
    const ShapeGroup& sg = static_cast<const ShapeGroup&>(shp);

    out << ShapeGroup::id << " " << sg.size() << endl;

    for(const auto& s : sg)
    {
        auto shape_rw = shape_rw_factory_.create(make_type_index(*s));
        shape_rw->write(*s, out);
    }
}
