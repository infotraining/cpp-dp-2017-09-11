#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <memory>
#include <vector>

#include "shape.hpp"

namespace Drawing
{
    class ShapeGroup : public CloneableShape<ShapeGroup>
    {
        std::vector<std::unique_ptr<Shape>> shapes_;
    public:
        static constexpr const char* id = "ShapeGroup";

        using iterator = std::vector<std::unique_ptr<Shape>>::iterator;
        using const_iterator = std::vector<std::unique_ptr<Shape>>::const_iterator;

        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source)
        {
            for(const auto& s : source.shapes_)
                shapes_.push_back(s->clone());
        }

        ShapeGroup& operator=(const ShapeGroup& source)
        {
            ShapeGroup temp(source);
            swap(temp);

            return *this;
        }

        void swap(ShapeGroup& other)
        {
            shapes_.swap(other.shapes_);
        }

        ShapeGroup(ShapeGroup&&) = default;
        ShapeGroup& operator=(ShapeGroup&&) = default;

        void draw() const override
        {
            for(const auto& s : shapes_)
                s->draw();
        }

        void move(int x, int y) override
        {
            for(const auto& s : shapes_)
                s->move(x, y);
        }

        void add(std::unique_ptr<Shape> s)
        {
            shapes_.push_back(std::move(s));
        }

        size_t size() const
        {
            return shapes_.size();
        }

        iterator begin()
        {
            return shapes_.begin();
        }

        iterator end()
        {
            return shapes_.end();
        }

        const_iterator begin() const
        {
            return shapes_.begin();
        }

        const_iterator end() const
        {
            return shapes_.end();
        }
    };
}

#endif // SHAPEGROUP_HPP
