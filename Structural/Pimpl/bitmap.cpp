#include "bitmap.hpp"
#include <array>

using namespace std;

struct Bitmap::BitmapImpl
{
    std::vector<char> image_;
};

Bitmap::Bitmap(size_t size, char fill_char) : impl_{make_unique<BitmapImpl>()}
{
    impl_->image_.resize(size, fill_char);
}

Bitmap::~Bitmap() = default;

void Bitmap::draw()
{
    cout << "Image: ";
    for(const auto pixel : impl_->image_)
        cout << pixel;
    cout << endl;
}
