#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <algorithm>
#include <iostream>
#include <vector>
#include <memory>

class Bitmap
{
    // forward declaration of private implementation
    class BitmapImpl;

    std::unique_ptr<BitmapImpl> impl_;
public:    
    Bitmap(size_t size, char fill_char = '*');
    ~Bitmap();

    Bitmap(const Bitmap&) = delete;
    Bitmap& operator=(const Bitmap&) = delete;

    Bitmap(Bitmap&&) = default;
    Bitmap& operator=(Bitmap&&) = default;

    void draw();
};



#endif // BITMAP_HPP
